<?php
define('T', 1);
define('ST', 0.5);
define('STEP', 1);
define('HALF_STEP', 0.5);

$major_scale = array(T, T, ST, T, T, T, ST);
$major_scale_steps = array('R', 'T', 'T', 'ST', 'T', 'T', 'T', 'ST');
$minor_scale = array(T, ST, T, T, ST, T, T);
$minor_scale_steps = array('R', 'T', 'ST', 'T', 'T', 'ST', 'T', 'T');

$root = 'C';
if (count($argv) > 1) {
    $tmp_root = strtoupper($argv[1]);
    $root = $tmp_root;
}

$scale_M = findScale($root, $major_scale);
printf("[%s] Scale\n", $root);
printScale($major_scale_steps);
printScale($scale_M);

$chords = buildChords($scale_M, $major_scale);

printChords($chords);

printf("[%sM] chord7\n", '');
$chord_7M = chord7($scale_M);
printScale($chord_7M);

print "\n";
printf("[%sm] Scale\n", $root);
$scale_m = findScale($root, $minor_scale);
printScale($minor_scale_steps);
printScale($scale_m);

$chords = buildChords($scale_m, $minor_scale);

printChords($chords);

printf("[%sm] chord7\n", '');
$chord_7m = chord7($scale_m);
printScale($chord_7m);




function findScale($root, $scale)
{
    $notes = array('C', 'D', 'E', 'F', 'G', 'A', 'B');

    $note_steps = array(
        'C' => array('D' => STEP),
        'D' => array('E' => STEP),
        'E' => array('F' => HALF_STEP),
        'F' => array('G' => STEP),
        'G' => array('A' => STEP),
        'A' => array('B' => STEP),
        'B' => array('C' => HALF_STEP)
    );

    $calculated_steps = array();

    $root_pos = findRootPosition($root, $notes);
    $scale_calculated = array();

    $scale_calculated[] = $root;
    $notes = tweakNotes($notes, $root_pos);

    $notes[] = $root;

    for ($i = 0; $i < (count($notes) -1); $i++) {
        $note = $notes[$i];
        $calculated_steps[$note][$notes[ $i + 1]] = $note_steps[$note][$notes[ $i + 1]];
    }

    //♭,♯
    for ($i = 1; $i <= count($scale); $i++) {
        $note = $notes[$i];

        if ($scale[$i - 1] != $calculated_steps[ $notes[$i - 1] ][ $notes[$i] ]) {
            if ($calculated_steps[ $notes[$i - 1] ][ $notes[$i] ] < $scale[$i - 1]) {
                $note = $notes[$i] . '♯';
                $calculated_steps[ $notes[$i - 1] ][ $notes[$i] ] += HALF_STEP;
                $calculated_steps[ $notes[$i] ][ $notes[$i + 1] ] -= HALF_STEP;
            } else {
                $note = $notes[$i] . '♭' ;
                //$calculated_steps[ $notes[$i - 1] ][ $notes[$i] ] += HALF_STEP;
                $calculated_steps[ $notes[$i - 1] ][ $notes[$i] ] -= HALF_STEP;
                $calculated_steps[ $notes[$i] ][ $notes[$i + 1] ] += HALF_STEP;
            }
        }
        $scale_calculated[] = $note;
    }
    return $scale_calculated;
}


function printScale($scale)
{
    foreach ($scale as $note) {
        printf("%-2s ", $note);
    }
    print "\n";
}

function findRootPosition($root = 'C', $notes = null)
{
    for ($i = 0; $i < count($notes); $i++) {
        if ($notes[$i] == $root) {
            return $i;
        }
    }
    return false;
}

// ♭,♯
function tweakNotes($notes, $position)
{
    $slice_start =  array_slice($notes, 0, $position);
    $slice_finish =  array_slice($notes, $position);
    return array_merge($slice_finish, $slice_start);
}

function chord($notes)
{
    $triad = array(1, 3, 5);
    $chord = array();
    foreach ($triad as $position) {
        $chord[] = $notes[$position - 1];
    }
    return $chord;
}

function buildChords($notes, $scale)
{
    array_pop($notes);
    $note_steps = array_combine($notes, $scale);
    $chords = array();
    for ($i = 0; $i < count($notes); $i++) {
        $note = $notes[$i];

        $root_pos = findRootPosition($note, $notes);
        $tmp_notes = tweakNotes($notes, $root_pos);
        // print_r($tmp_notes);
        $scale_steps = array();
        for ($j = 0; $j < count($tmp_notes); $j++) {
            $scale_steps[$tmp_notes[$j]] = $note_steps[$tmp_notes[$j]];
        }

        $chord = chord($tmp_notes);
        $chords[$note]['chord'] = $chord;
        $chords[$note]['type'] = chordType($chord, $scale_steps);
    }
    return $chords;
}

function chordType($chord, $note_steps)
{
    $steps = 0;
    $note = $chord[0];
    next($note_steps);
    $next_note = key($note_steps);
    $steps += $note_steps[$note];
    $steps += $note_steps[$next_note];
    if ($steps == 2) {
        return 'major';
    } else {
        return 'minor';
    }
}

function chord7($notes)
{
    $triad = array(1, 3, 5, 7);
    $chord = array();
    foreach ($triad as $position) {
        $chord[] = $notes[$position - 1];
    }
    return $chord;
}

function printChords($chords)
{
    foreach ($chords as $note => $info) {
        printf("Chord %s %s\n ", $note, $info['type']);
        print(implode(' ', $info['chord']) . "\n");
    }
}
